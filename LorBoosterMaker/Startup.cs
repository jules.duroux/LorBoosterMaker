using LorBoosterMaker.Domain;
using LorBoosterMaker.Domain.Restrictions;
using LorBoosterMaker.Repositories;
using LorBoosterMaker.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;

namespace LorBoosterMaker
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins,
                                  builder =>
                                  {
                                      builder.WithOrigins("*").AllowAnyHeader()
                                                  .AllowAnyMethod();
                                  });
            });

            services.AddHttpClient<LorGuardianServices>(client =>
            {
                client.BaseAddress = new Uri("https://backend.lorguardian.com");
            }).ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
            {
                AllowAutoRedirect = false,
                AutomaticDecompression = DecompressionMethods.All
            });

            services.AddSingleton<PackRepository>();
            var defaultGeneralRestrictions = new GeneralRestriction(3, 33);
            var defaultChampionRestrictions = new CardRarityByRegion("Champion", 5, 5);
            var defaultCreatureRestrictions = new CardTypeByRegion("Unit", 15, null);
            var defaultSpellRestrictions = new CardTypeByRegion("Spell", 8, null);
            var defaultRegionRestrictions = new List<IRegionRestriction>
            {
                defaultChampionRestrictions,
                defaultCreatureRestrictions,
                defaultSpellRestrictions,
                new CardCostByRegion(0, 0, 1),
                new CardCostByRegion(1, 3, null),
                new CardCostByRegion(2, 3, null),
                new CardCostByRegion(3, 3, null),
                new CardCostByRegion(4, 3, null),
                new CardCostByRegion(5, 2, null),
                new CardCostByRegion(6, 1, null),
                new CardCostByRegion(7, 1, null),
                new CardCostByRegion(8, 1, null)
        };
            var restrictionsRepository = new RestrictionsRepository(defaultRegionRestrictions, defaultGeneralRestrictions);
            services.AddSingleton(restrictionsRepository);
            services.AddSingleton<PackBuilder>();
            services.AddSingleton<CollectionService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "LorBoosterMaker", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "LorBoosterMaker v1"));

            app.UseRouting();

            app.UseCors(MyAllowSpecificOrigins);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
