﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace LorBoosterMaker.Domain
{
    public class Card
    {
        public Card(string regionRef, IEnumerable<Asset> assets, string name, string cardCode, string rarityRef, int nbAvailable, int cost, string set, string type, string superType)
        {
            RegionRef = regionRef;
            Assets = assets;
            Name = name;
            CardCode = cardCode;
            RarityRef = rarityRef;
            NbAvailable = nbAvailable;
            Cost = cost;
            Set = set;
            Type = type;
            SuperType = superType;
        }

        [JsonPropertyName("regionRef")]
        public string RegionRef { get; set; }

        [JsonPropertyName("assets")]
        public IEnumerable<Asset> Assets { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("cardCode")]
        public string CardCode { get; set; }

        [JsonPropertyName("rarityRef")]
        public string RarityRef { get; set; }

        [JsonPropertyName("count")]
        public int NbAvailable { get; set; }

        [JsonPropertyName("cost")]
        public int Cost { get; set; }

        [JsonPropertyName("set")]
        public string Set { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("supertype")]
        public string SuperType { get; set; }
    }
}
