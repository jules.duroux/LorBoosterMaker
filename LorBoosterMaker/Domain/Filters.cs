﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace LorBoosterMaker.Services
{
    public class Filters
    {
        public Filters(List<string>? regionRef, List<string>? type, List<string>? rarityRef, List<int>? cost, List<int>? set, string ownedCard, List<string>? superTypes, string owned)
        {
            RegionRef = regionRef;
            Type = type;
            RarityRef = rarityRef;
            Cost = cost;
            Set = set;
            OwnedCard = ownedCard;
            SuperTypes = superTypes;
            Owned = owned;
        }

        [JsonPropertyName("card.regionRef")]
        public List<string>? RegionRef { get; set; }

        [JsonPropertyName("card.type")]
        public List<string>? Type { get; set; }

        [JsonPropertyName("card.rarityRef")]
        public List<string>? RarityRef { get; set; }

        [JsonPropertyName("card.cost")]
        public List<int>? Cost { get; set; }

        [JsonPropertyName("set")]
        public List<int>? Set { get; set; }

        [JsonPropertyName("card.owned")]
        public string OwnedCard { get; set; }

        [JsonPropertyName("card.supertype")]
        public List<string>? SuperTypes { get; set; }

        [JsonPropertyName("owned")]
        public string Owned { get; set; }
    }
}