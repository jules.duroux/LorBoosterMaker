﻿using System.Collections.Generic;
namespace LorBoosterMaker.Domain.Restrictions
{
    public interface IRegionRestriction
    {
        public int? Min { get; }
        public int? Max { get; }

        public bool Match(Card card);
    }
}
