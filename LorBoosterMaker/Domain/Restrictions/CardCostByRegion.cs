﻿namespace LorBoosterMaker.Domain.Restrictions
{
    public class CardCostByRegion : IRegionRestriction
    {
        public CardCostByRegion(int cost, int? min, int? max)
        {
            Cost = cost;
            Min = min;
            Max = max;
        }

        public int Cost { get; set; }
        public int? Min { get; set; }
        public int? Max { get; set; }

        public bool Match(Card card)
        {
            return card.Cost == Cost;
        }
    }
}
