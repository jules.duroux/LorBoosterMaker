﻿namespace LorBoosterMaker.Domain.Restrictions
{
    public class CardRarityByRegion : IRegionRestriction
    {
        public CardRarityByRegion(string rarityRef, int? min, int? max)
        {
            RarityRef = rarityRef;
            Min = min;
            Max = max;
        }

        public string RarityRef { get; set; }
        public int? Min { get; set; }
        public int? Max { get; set; }

        public bool Match(Card card)
        {
            return card.RarityRef == RarityRef;
        }
    }
}
