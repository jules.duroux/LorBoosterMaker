﻿namespace LorBoosterMaker.Domain.Restrictions
{
    public class GeneralRestriction
    {
        public GeneralRestriction(int regions, int cardsByRegion)
        {
            Regions = regions;
            CardsByRegion = cardsByRegion;
        }

        public int Regions { get; set; }
        public int CardsByRegion { get; set; }
    }
}
