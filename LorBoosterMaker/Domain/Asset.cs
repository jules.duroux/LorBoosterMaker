﻿using System.Text.Json.Serialization;

namespace LorBoosterMaker.Domain
{
    public class Asset
    {
        public Asset(string gameAbsolutePath, string fullAbsolutePath)
        {
            GameAbsolutePath = gameAbsolutePath;
            FullAbsolutePath = fullAbsolutePath;
        }

        [JsonPropertyName("gameAbsolutePath")]
        public string GameAbsolutePath { get; set; }

        [JsonPropertyName("fullAbsolutePath")]
        public string FullAbsolutePath { get; set; }
    }
}