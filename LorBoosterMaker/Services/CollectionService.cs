﻿using LorBoosterMaker.Domain;
using LorBoosterMaker.Repositories;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LorBoosterMaker.Services
{
    public class CollectionService
    {
        private readonly LorGuardianServices _lorGuardianServices;
        private readonly PackRepository _packRepository;
        private readonly PackBuilder _packBuilder;

        public CollectionService(LorGuardianServices lorGuardianServices, PackRepository packRepository, PackBuilder packBuilder)
        {
            _lorGuardianServices = lorGuardianServices;
            _packRepository = packRepository;
            _packBuilder = packBuilder;
        }

        public async Task<CollectionResponse> GetFilteredPackAsync(CollectionRequest request, string authToken, string magic, CancellationToken cancellationToken)
        {
            if(request.Page > 0)
            {
                return new CollectionResponse(new List<Card>());
            }
            var pack = _packRepository.Get(authToken);
            if (pack is null)
            {
                var collection = await _lorGuardianServices.GetCollectionAsync(authToken, magic, cancellationToken);
                pack = _packBuilder.BuildPack(authToken, collection);
                _packRepository.Add(pack);
            }

            return new CollectionResponse(pack.GetFitered(request.Filters));
        }
    }
}
