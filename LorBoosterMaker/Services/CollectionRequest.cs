﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace LorBoosterMaker.Services
{
    public class CollectionRequest
    {
        public CollectionRequest(int page, int pageSize, Filters filters)
        {
            Page = page;
            PageSize = pageSize;
            Filters = filters;
        }

        [JsonPropertyName("page")]
        public int Page { get; set; }

        [JsonPropertyName("pageSize")]
        public int PageSize { get; set; }

        [JsonPropertyName("filters")]
        public Filters Filters { get; set; }

        public static CollectionRequest GetFullCollectionRequest()
        {
            var emptystringFilter = new List<string>();
            var emptyIntFilter = new List<int>();
            var filters = new Filters(emptystringFilter, emptystringFilter, emptystringFilter, emptyIntFilter, emptyIntFilter, "owned", emptystringFilter, "owned");
            return new CollectionRequest(0, 2000, filters);
        }
    }
}
