﻿using LorBoosterMaker.Domain;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace LorBoosterMaker.Services
{
    public class LorGuardianServices
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<LorGuardianServices> _logger;
        private const string collectionUrl = "cards/pagination";

        public LorGuardianServices(HttpClient httpClient, ILogger<LorGuardianServices> logger)
        {
            _httpClient = httpClient;
            _logger = logger;
        }

        public async Task<IEnumerable<Card>> GetCollectionAsync(string authToken, string magic, CancellationToken cancellationToken)
        {
            var request = CollectionRequest.GetFullCollectionRequest();

            using var requestMessage = new HttpRequestMessage(HttpMethod.Post, new Uri(collectionUrl, UriKind.Relative));
            requestMessage.Headers.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0");
            requestMessage.Headers.TryAddWithoutValidation("Accept", "application/json, text/plain, */*");
            requestMessage.Headers.TryAddWithoutValidation("Accept-Language", "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3");
            requestMessage.Headers.TryAddWithoutValidation("Referer", "https://www.lorguardian.com/");
            requestMessage.Headers.TryAddWithoutValidation("magic", magic);
            requestMessage.Headers.TryAddWithoutValidation("Origin", "https://www.lorguardian.com");
            requestMessage.Headers.TryAddWithoutValidation("Connection", "keep-alive");
            requestMessage.Content = new StringContent(JsonSerializer.Serialize(request));
            
            requestMessage.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", authToken);

            var response = await _httpClient.SendAsync(requestMessage, cancellationToken);

            var collectionAsString = await response.Content.ReadAsStringAsync(cancellationToken);
            var result = JsonSerializer.Deserialize<CollectionResponse>(collectionAsString);
            if (result is null)
            {
                var error = "Something went wrong when fetching collection on LorGuardian";
                _logger.LogError(error);
                throw new InvalidOperationException(error);
            }
            return result.Cards;
        }
    }
}
