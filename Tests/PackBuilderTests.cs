using FluentAssertions;
using LorBoosterMaker.Domain;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Tests
{
    public class PackBuilderTests
    {
        [Theory]
        [AutoMoqData]
        public void DissociateCopiesAndShuffle_Should_Create_As_Many_Card_As_NbAvailable(IEnumerable<Card> cards)
        {
            //Arrange
            var i = 1;
            foreach (var card in cards)
            {
                card.NbAvailable = i;
                i++;
            }

            //Act
            var listCards = PackBuilder.DissociateCopiesAndShuffle(new List<Card>(cards));

            //Assert
            cards.Count().Should().Be(3);
            listCards.Count().Should().Be(1 + 2 + 3);
            listCards.Distinct().Select(c => c.Name).Should().BeEquivalentTo(cards.Select(c => c.Name));
        }
    }
}
